const getSum = (str1, str2) => {
  var result;
  if (str1 === "")
    str1 = "0";
  if (str2 === "")
    str2 = "0";

  if (typeof str1 === "string" && typeof str2 === "string" && !isNaN(str1) && !isNaN(str2)) {
    result = parseInt(str1) + parseInt(str2);
    return result.toString();
  } else {
    return false;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var postCount = 0;
  var commentCount = 0;

  for (let i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i].author === authorName) {
      postCount++;
    }
    if (listOfPosts[i].comments !== undefined) {
      for (let j = 0; j < listOfPosts[i].comments.length; j++) {
        if (listOfPosts[i].comments[j].author === authorName) {
          commentCount++;
        }
      }
    }
  }
  return "Post:" + postCount + ",comments:" + commentCount;
};

const tickets=(people)=> {
  var sum = 0;
  for (let i = 0; i < people.length; i++) {
    if (people[i] === 25) {
      sum += 25;
    } else if ((sum - (people[i] - 25)) < 0) {
      return "NO";
    } else if ((sum - (people[i] - 25)) >= 0){
      sum = sum + people[i] - (people[i] - 25);
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
